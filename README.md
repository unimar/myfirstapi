# My First API

Repositório dedicado para o projeto e testes da disciplina **Desenvolvimento de Aplicações para Internet** no curso de **Análise e Desenvolvimento de Sistemas** da _Universidade de Marília (Unimar)_

## Desenvolvimento de Aplicações para Internet

A disciplina se dedica ao entendimento teórico e desenvolvimento prático dos conceitos relacionados ao desenvolvimento de software focados para o ambiente Web. 

Como ferramenta para ensino e desenvolvimento do projetos, está sendo utilizada a Linguagem PHP e o framework Laravel.

## Projeto

O projeto de teste e utilizado como case para disciplina se dedica a uma **API**(_Application Programming Interface_) de tarefas. Se baseando nos padrões de Rest, ou seja, utilizando um endpoint por recurso necessário e trabalhando os verbos HTTP (Get, Post, Put e Delete).

Dessa forma temos ao final do projeto o recurso **tasks** com as seguintes rotas:

```bash
GET|HEAD ...... api/tasks
POST .......... api/tasks
GET|HEAD ...... api/tasks/{task}
PUT|PATCH ..... api/tasks/{task}
DELETE ........ api/tasks/{task}
```

## How To

Requisitos para desenvolver:

> PHP 8+
>
> Composer
>
> MySQL Server

### Criar projeto laravel

1. Acessar o diretório que vai criar seu projeto
2. Executar o comando _composer_ para criar o projeto

```bash
composer create-project laravel/laravel --prefer-dist <nomeDoProjeto>
```
> Nesse passo será criado o projeto com todas as dependências necessárias

3. Acessar a pasta do projeto que foi criada

```bash
cd <nomeDoProjeto>
```

4. Editar o arquivo `.env` para suas variáveis de conexão com o banco
> Conforme as configurações de banco da sua máquina, assim pode variar

```env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<seuBancoAqui>
DB_USERNAME=<seuUsuarioDoBanco>
DB_PASSWORD=<senhaDoUsuarioDoBanco>
```

> Com o projeto criado e o ambiente configurado, vamos criar nosso migration e preparar o banco

5. Criar a migration, no terminal dentro do projeto criado, execute:

```bash
php artisan make:migration create_<nome_tabela>_table
```
> substituir <nome_tabela> pelo nome da tabela no plural. Ex: tasks
>
> vai criar um arquivo no diretório database/migrations/...create_<nome_tabela>_table.php

6. Editar o arquivo criado conforme os campos que sua tabela terá no banco

Ex:

```php
/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('date');
            $table->boolean('status');
            $table->timestamps();
        });
    }
```
> Com a migration criada, precisamos enviar essa estrutura de tabela para o banco

7. Enviar a migration para o banco, assim será criada a tabela com o nome e campos necessários. Executar no terminal

```bash
php artisan migrate
```
> Conferir o banco se criou devidamente a tabela (Irão ser criadas outras tabelas por padrão)

8. Agora precisamos criar noss estrutura do projeto com Controller e Model. No terminal:

```bash
php artisan make:controller Api\\TaskController --model=Task --api
```
> Se pedir para criar o arquivo, dê permissão
>
> Com esse comando já são criadas a Controller e a Model, já linkadas
>
> O comando --api serve para já criar as funções necessárias para API na Controller criada